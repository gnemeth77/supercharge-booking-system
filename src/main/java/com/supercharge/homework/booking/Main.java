package com.supercharge.homework.booking;

import java.time.LocalDate;
import java.util.List;

import com.supercharge.homework.booking.dao.ReservationDao;
import com.supercharge.homework.booking.dao.RoomDao;
import com.supercharge.homework.booking.dao.UserDao;
import com.supercharge.homework.booking.entity.Reservation;
import com.supercharge.homework.booking.entity.Room;
import com.supercharge.homework.booking.entity.User;
import com.supercharge.homework.booking.exception.BookingBusinessException;
import com.supercharge.homework.booking.exception.BookingTechnicalException;
import com.supercharge.homework.booking.service.ReservationService;

public class Main {

    public static void main(String[] args) {
        try {
            UserDao userDao = new UserDao();
            User user1 = userDao.create("user1");
            User user2 = userDao.create("user2");

            RoomDao roomDao = new RoomDao();
            Room room1 = roomDao.create("Room1", "Budapest, Teszt utca 1.", 10000);
            Room room2 = roomDao.create("Room2", "Budapest, Teszt Elek tér 5.", 20000);
            
            ReservationDao reservationDao = new ReservationDao();
            
            ReservationService reservationService = new ReservationService(roomDao, reservationDao);
            
            reservationService.create(user1, room1, LocalDate.now(), LocalDate.now().plusDays(7));
            reservationService.create(user1, room2, LocalDate.now().plusDays(8), LocalDate.now().plusDays(15));
            reservationService.printHistory(user1);

            //try to book room which is not available
            try {
                reservationService.create(user2, room1, LocalDate.now(), LocalDate.now().plusDays(7));
            } catch(BookingBusinessException e) {
                System.out.println(e.getBookingExceptionCode());
            }

            //test cancellation
            Reservation reservation = reservationService.create(user2, room2, LocalDate.now(), LocalDate.now().plusDays(7));
            reservationService.create(user2, room1, LocalDate.now().plusDays(8), LocalDate.now().plusDays(15));
            reservationService.cancel(reservation);
            reservationService.printHistory(user2);

            //list rooms
            List<Room> rooms = reservationService.getAvailableRooms(0, LocalDate.now(), LocalDate.now().plusDays(7));
            for(Room r : rooms) {
                System.out.println(r.getName()+" "+r.getAddress()+" "+r.getDailyPrice()+"HUF");
            }
            //list rooms by price+date
            List<Room> rooms2 = reservationService.getAvailableRooms(10000, LocalDate.now(), LocalDate.now().plusDays(7));
            for(Room r : rooms2) {
                System.out.println(r.getName()+" "+r.getAddress()+" "+r.getDailyPrice()+"HUF");
            }

        } catch (Throwable e) {
            e.printStackTrace();
            throw new BookingTechnicalException(e.getMessage());
        } 
    }

}
