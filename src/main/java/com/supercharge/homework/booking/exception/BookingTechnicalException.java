package com.supercharge.homework.booking.exception;

public class BookingTechnicalException extends RuntimeException{
    static final long serialVersionUID = 1L;

    String technicalErrorCode;

    public String getTechnicalErrorCode() {
        return technicalErrorCode;
    }

    public void setTechnicalErrorCode(String technicalErrorCode) {
        this.technicalErrorCode = technicalErrorCode;
    }

    public BookingTechnicalException(String technicalErrorCode) {
        super();
        this.technicalErrorCode = technicalErrorCode;
    }

    
}
