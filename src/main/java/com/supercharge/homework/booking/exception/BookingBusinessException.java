package com.supercharge.homework.booking.exception;

public class BookingBusinessException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private String bookingExceptionCode;

    public BookingBusinessException(String bookingExceptionCode) {
        super();
        this.bookingExceptionCode = bookingExceptionCode;
    }

    public String getBookingExceptionCode() {
        return bookingExceptionCode;
    }

    public void setBookingExceptionCode(String bookingExceptionCode) {
        this.bookingExceptionCode = bookingExceptionCode;
    }

}
