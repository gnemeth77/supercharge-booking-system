package com.supercharge.homework.booking.entity;

public class User extends Entity {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
