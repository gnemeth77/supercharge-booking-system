package com.supercharge.homework.booking.entity;

public class Room extends Entity{
    private String name;
    private String address;
    private int dailyPrice;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public int getDailyPrice() {
        return dailyPrice;
    }
    public void setDailyPrice(int dailyPrice) {
        this.dailyPrice = dailyPrice;
    }

}
