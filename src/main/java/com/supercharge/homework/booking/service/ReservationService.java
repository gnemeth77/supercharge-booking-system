package com.supercharge.homework.booking.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.supercharge.homework.booking.dao.ReservationDao;
import com.supercharge.homework.booking.dao.RoomDao;
import com.supercharge.homework.booking.entity.Reservation;
import com.supercharge.homework.booking.entity.Room;
import com.supercharge.homework.booking.entity.User;
import com.supercharge.homework.booking.exception.BookingBusinessException;


public class ReservationService {
    private RoomDao roomDao;
    private ReservationDao reservationDao;

    public ReservationService(RoomDao roomDao, ReservationDao reservationDao) {
        super();
        this.roomDao = roomDao;
        this.reservationDao = reservationDao;
    }

    /**
     * List rooms based on price and date period
     * @param maxprice
     * @param fromDate
     * @param toDate
     * @return rooms
     */
    public List<Room> getAvailableRooms(int maxprice, LocalDate fromDate, LocalDate toDate) {
        checkNotNull(fromDate);
        checkNotNull(toDate);
        if(maxprice < 1) {
            maxprice = Integer.MAX_VALUE;
        }
        List<Room> availableRooms = new ArrayList<>();
        
        for(Room r :roomDao.getAll()) {
            if(isRoomAvailable(r, fromDate, toDate)
               && (r.getDailyPrice() < maxprice)) {
                availableRooms.add(r);
            }
        }
        return availableRooms;
    }

    /**
     * Book a free room
     * @param user
     * @param room
     * @param fromDate
     * @param toDate
     * @return
     */
    public Reservation create(User user, Room room, LocalDate fromDate, LocalDate toDate) {
        checkNotNull(user);
        checkNotNull(room);
        checkNotNull(fromDate);
        checkNotNull(toDate);
        //check the room if it is available
        if(!isRoomAvailable(room, fromDate, toDate)) {
            throw new BookingBusinessException("ROOM_NOT_AVAILABLE");
        }
        long price = calculatePrice(room, fromDate, toDate);
        return reservationDao.create(user, room, fromDate, toDate, price);
    }

    /**
     * Cancel an existing reservation
     * @param reservation
     */
    public void cancel(Reservation reservation) {
        checkNotNull(reservation);
        checkNotNull(reservation.getId());
        reservationDao.deleteById(reservation.getId());
    }

    /**
     * List the user's full booking history
     * @param user
     * @return
     */
    public List<Reservation> getFullHistory(User user) {
        checkNotNull(user);
        List<Reservation> reservations = new ArrayList<>();
        for(Reservation r : reservationDao.getAll()) {
            if(r.getUserId().equals(user.getId())) {
                reservations.add(r);
            }
        }
        return reservations;
    }

    /**
     * List the user's booking history by date and price filters
     * @param user
     * @param date
     * @param price
     * @return
     */
    public List<Reservation> getHistoryByFilters(User user, LocalDate date, long price) {
        if(price < 1) {
            price = Integer.MAX_VALUE;
        }
        List<Reservation> reservations = new ArrayList<>();
        for(Reservation r : getFullHistory(user)) {
            if(date!=null && !date.isBefore(r.getFromDate()) && !date.isAfter(r.getToDate())) {
                if(r.getPrice()<price) {
                    reservations.add(r);
                }
            }else {
                if(r.getPrice()<price) {
                    reservations.add(r);
                }
            }
        }
        return reservations;
    }

    /**
     * Print the user's full booking history
     * @param user
     */
    public void printHistory(User user) {
        for(Reservation r : getFullHistory(user)) {
            Room room = roomDao.getById(r.getRoomId());
            System.out.println(room.getName()+" "+ room.getAddress()+ " " +r.getFromDate() +" "+r.getToDate()+" "+r.getPrice()+"HUF");
        }
    }

    /**
     * Checks is if a room is available in a date period
     * @param room
     * @param fromDate
     * @param toDate
     * @return
     */
    private boolean isRoomAvailable(Room room, LocalDate fromDate, LocalDate toDate) {
        for(Reservation r : reservationDao.getAll()) {
            if(r.getRoomId().equals(room.getId())
               && isOverlapping(fromDate, toDate, r.getFromDate(), r.getToDate())) {
                return false;
            }
        }
        return true;
    }

    private boolean isOverlapping(LocalDate start1, LocalDate end1, LocalDate start2, LocalDate end2) {
        return start1.isBefore(end2) && start2.isBefore(end1);
    }

    private long calculatePrice(Room room, LocalDate fromDate, LocalDate toDate) {
        return room.getDailyPrice() * ChronoUnit.DAYS.between(fromDate, toDate);
    }

    private void checkNotNull(Object o) {
        if(o == null) {
            throw new BookingBusinessException("INVALID_INPUT_PARAM");
        }
    }
}
