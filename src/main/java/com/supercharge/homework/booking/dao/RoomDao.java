package com.supercharge.homework.booking.dao;

import com.supercharge.homework.booking.entity.Room;

public class RoomDao extends BaseDao<Room>{

    public Room create(String name, String address, int dailyPrice) {
        Room room = super.create(Room.class);
        room.setName(name);
        room.setAddress(address);
        room.setDailyPrice(dailyPrice);
        return room;
    }

}
