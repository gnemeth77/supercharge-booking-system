package com.supercharge.homework.booking.dao;

import com.supercharge.homework.booking.entity.User;

public class UserDao extends BaseDao<User>{
    public User create(String username) {
        User user = super.create(User.class);
        user.setUsername(username);
        return user;
    }
}
