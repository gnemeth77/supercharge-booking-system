package com.supercharge.homework.booking.dao;

import java.time.LocalDate;

import com.supercharge.homework.booking.entity.Reservation;
import com.supercharge.homework.booking.entity.Room;
import com.supercharge.homework.booking.entity.User;

public class ReservationDao extends BaseDao<Reservation>{

    public Reservation create(User user, Room room, LocalDate fromDate, LocalDate toDate, long price){
        Reservation reservation = super.create(Reservation.class);
        reservation.setUserId(user.getId());
        reservation.setRoomId(room.getId());
        reservation.setFromDate(fromDate);
        reservation.setToDate(toDate);
        reservation.setPrice(price);
        return reservation;
    }
}
