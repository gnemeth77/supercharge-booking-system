package com.supercharge.homework.booking.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.supercharge.homework.booking.entity.Entity;
import com.supercharge.homework.booking.exception.BookingTechnicalException;

public class BaseDao<T extends Entity> {

    protected Map<UUID, T> store = new HashMap<>();
    
    protected T create(Class<T> clazz){
        try {
            T entity = clazz.newInstance();
            UUID id = UUID.randomUUID();
            entity.setId(id);
            store.put(id, entity);
            return entity;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw new BookingTechnicalException(e.getMessage());
        }
    }

    public void deleteById(UUID id) {
        store.remove(id);
    }

    public T getById(UUID id) {
        return store.get(id);
    }

    public Collection<T> getAll() {
        return store.values();
    }
    
}
